from random import randint
from tkinter.messagebox import YES
name = input("Hi! What is your name?")

knows_birthday = False



for num in range(5):
    if(knows_birthday == False):
        random_month = randint(1, 12)
        random_year = randint(1924, 2004)
        print("Guess", num+1, name, "were you born in", random_month, "/", random_year, "?")
        answer = input("yes or no?")
        if(answer == "yes"):
            knows_birthday = True
            print("I knew it!")
        elif(answer == "no"):
            print("Drat! Lemme try again!")
        else:
            print("not a valid input. I'll guess again. Please answer yes or no")


if (knows_birthday == True):
    print("I win! I guessed your birthday.")
else:
    print("You win! I couldn't guess your birthday.")
